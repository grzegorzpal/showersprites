using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DropdownHandle : MonoBehaviour
{
    [SerializeField] private TMP_InputField m_folderNameInputField;

    private bool m_loadFromResources = true;
    public bool LoadFromResources => m_loadFromResources;
    public void ChangeValue(TextMeshProUGUI label)
    {
        m_loadFromResources = label.text == "Resources" ? true : false;

        m_folderNameInputField.gameObject.SetActive(!m_loadFromResources);
    }
}
