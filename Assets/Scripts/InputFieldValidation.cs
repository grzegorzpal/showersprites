using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;

public class InputFieldValidation : MonoBehaviour
{
    private const string ASSETS_PATH = "Assets";
    private const string SPECIAL_CHAR = @"\";

    [SerializeField] private Button m_refreshButton;
    [SerializeField] private Image m_inputFieldImage;
    [SerializeField] private TMP_InputField m_inputField;
    [SerializeField] private TextMeshProUGUI m_textInfo;
    [SerializeField] private string m_existsFolderInfo;
    [SerializeField] private string m_dosentExistsFolderInfo;

    private void OnDisable()
    {
        m_inputFieldImage.color = Color.white;
        m_inputField.text = string.Empty;
        m_textInfo.text = string.Empty;
        m_refreshButton.interactable = true;
    }
    public void ValidateFolderName()
    {
        string[] allDirectoriesPaths = Directory.GetDirectories(ASSETS_PATH, "*", SearchOption.AllDirectories);

        List<string> allDirectoriesName = new List<string>();

        for (int i = 0; i < allDirectoriesPaths.Length; i++)
        {
            int lastSlashPos = allDirectoriesPaths[i].LastIndexOf(SPECIAL_CHAR) + 1;
            allDirectoriesName.Add(allDirectoriesPaths[i].Substring(lastSlashPos, allDirectoriesPaths[i].Length - lastSlashPos));
        }

        if (allDirectoriesName.Contains(m_inputField.text))
        {
            m_refreshButton.interactable = true;
            m_inputFieldImage.color = Color.green;
            m_textInfo.text = m_existsFolderInfo;
        }
        else
        {
            m_refreshButton.interactable = false;
            m_inputFieldImage.color = Color.red;
            m_textInfo.text = m_dosentExistsFolderInfo;
        }
    }
}
