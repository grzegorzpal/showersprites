using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEditor;
using TMPro;

public class ItemCreator : MonoBehaviour
{
    private const float SIZE_OF_DELTA_X = 500;
    private const float SIZE_OF_DELTA_Y = 100;
    private const string ASSET_PATH = "Assets/";

    [SerializeField] private DropdownHandle m_dropdownHandle;
    [SerializeField] private TMP_InputField m_textInputFolderName;
    [SerializeField] private Item m_itemPrefab;
    [SerializeField] private RectTransform m_scrollViewContentRect;
    [SerializeField] private Scrollbar m_scrollbar;
    [SerializeField] private TextMeshProUGUI m_textInfo;
    [SerializeField] private string m_zeroTexuresInFolder;

    private List<string> m_itemsName = new List<string>();
    private List<Item> m_items = new List<Item>();
    private void Start()
    {
        RefreshItems();
        m_scrollbar.value = 1;
    }
    public void RefreshItems()
    {
        for (int i = 0; i < m_items.Count; i++)
        {
            Destroy(m_items[i].gameObject);
        }
        m_items.Clear();
        m_itemsName.Clear();

        List<Texture2D> textures = GetTextures(m_dropdownHandle.LoadFromResources);

        m_textInfo.text = textures.Count == 0 ? m_zeroTexuresInFolder : string.Empty;

        m_scrollViewContentRect.sizeDelta = new Vector2(SIZE_OF_DELTA_X, textures.Count * SIZE_OF_DELTA_Y);

        for (int i = 0; i < textures.Count; i++)
        {
            if (m_itemsName.Contains(textures[i].name)) continue;

            CreateItem(textures[i], i);
        }
        m_scrollbar.value = 1;
    }


    private List<Texture2D> GetTextures(bool loadFromResources)
    {
        if (loadFromResources)
        {
            return Resources.LoadAll<Texture2D>("").ToList();
        }
        else
        {
            return new List<Texture2D>().ListOfObjects<Texture2D>(ASSET_PATH + m_textInputFolderName.text);
        }
    }
    
    private void CreateItem(Texture2D texture, int index)
    {
        Item item = Instantiate(m_itemPrefab, transform);
        item.SetItemPriporties(texture);
        item.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, index * (-SIZE_OF_DELTA_Y));
        m_itemsName.Add(item.NameItem);
        m_items.Add(item);
    }
}

