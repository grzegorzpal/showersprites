using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public static class ExtensionMethods
{
    public static List<T> ListOfObjects<T>(this List<T> listOfObjects, string path)
    {
        string[] filePaths = Directory.GetFiles(path);

        List<UnityEngine.Object> objects = new List<UnityEngine.Object>();

        if (filePaths != null && filePaths.Length > 0)
        {
            for (int i = 0; i < filePaths.Length; i++)
            {
                if (!filePaths[i].Contains(".meta"))
                {
                    objects.Add(AssetDatabase.LoadAssetAtPath(filePaths[i], typeof(UnityEngine.Object)));
                }
            }
        }

        List<T> listOfTObjects = new List<T>();

        for (int i = 0; i < objects.Count; i++)
        {
            if (objects[i].GetType() == typeof(T))
            {
                T result = (T)Convert.ChangeType(objects[i], typeof(T));
                listOfTObjects.Add(result);
            }
        }
        
        return listOfTObjects;
    }
}

